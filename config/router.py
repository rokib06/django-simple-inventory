from inventory.viewsets import ProductViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('product', ProductViewset)

# localhost:p/api/
# GET, POST, UPDATE, DELETE
# List. retrieve


