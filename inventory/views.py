from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
# specific to this view
from django.shortcuts import render, reverse

from django.views import generic
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

from .models import Product

from django.urls import reverse_lazy


class ProductListView(generic.ListView):
    model = Product
    template_name = 'inventory/product/list.html'
    context_object_name = 'latest_product_list'

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        return context


class ProductDetailView(generic.DetailView):

    model = Product
    template_name = 'inventory/product/detail.html'
    context_object_name = 'product'


class ProductCreateView(generic.CreateView):
    model = Product
    template_name = 'inventory/product/create.html'
    fields = ('title', 'description', 'price', 'amount')
    success_url = reverse_lazy('product-list')


class ProductUpdateView(generic.UpdateView):

    model = Product
    template_name = 'inventory/product/update.html'
    context_object_name = 'product'
    fields = ('title', 'description', 'price', 'amount',)

    def get_success_url(self):
        return reverse('product-detail', kwargs={'pk': self.object.id})


class ProductDeleteView(generic.DeleteView):
    model = Product
    template_name = 'inventory/product/delete.html'
    success_url = reverse_lazy('product-list')


def pagination(request):
    product_list = Product.objects.all()
    paginator = Paginator(product_list, 3) # records per page
    page = request.GET.get('page')

    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        items = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        items = paginator.page(paginator.num_pages)

    return render(request, 'inventory/product/pagignation.html', {'items': items })
