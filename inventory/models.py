from django.db import models


class Product(models.Model):
	title = models.CharField(max_length=100)
	description = models.CharField(max_length=300)
	amount = models.IntegerField()
	price = models.FloatField()
	# pub_date = models.DateTimeField('date published')

	def __str__(self):
		return self.title
	# Create / insert/ add -post

	# Retrive / fetch get

	# update/edit -put

	# delete / remove - delete
	
