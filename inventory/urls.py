from django.urls import path
from . import views


urlpatterns =[
	path('', views.ProductListView.as_view(), name='product-list'),
	# path('', views.index, name='index')
	path('product/<int:pk>', views.ProductDetailView.as_view(), name='product-detail'),
	path('product/create', views.ProductCreateView.as_view(), name='product-create'),
	path('product/<int:pk>/update', views.ProductUpdateView.as_view(), name='product-update'),
	path('product/<int:pk>/delete', views.ProductDeleteView.as_view(), name='product-delete'),
	path('test/', views.pagination, name='product-page'),
]